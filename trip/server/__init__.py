from flask import Flask, Blueprint
from flask_restx import Api as RestX_Api
from server.details.v1 import details_v1


api_blueprint = Blueprint("api", __name__, url_prefix="/trip/api")


restx_api = RestX_Api(api_blueprint, title="Go Ride trip")

restx_api.add_namespace(details_v1, path="/v1")


def create_app():
    app = Flask(__name__)
    app.config.from_object("server.config")
    app.register_blueprint(api_blueprint)
    return app
