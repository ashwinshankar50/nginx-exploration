import logging

TRIP_DATA = {
    1: {
        "driver_id": 1,
        "passenger_id": 1,
        "fare": 200,
        "distance": {"km": 22, "mts": 200},
        "time": {"m": 45, "h": 1},
        "from": "A",
        "to": "B",
    },
    2: {
        "driver_id": 2,
        "passenger_id": 1,
        "fare": 201,
        "distance": {"km": 22, "mts": 200},
        "time": {"m": 45, "h": 1},
        "from": "B",
        "to": "A",
    },
    3: {
        "driver_id": 1,
        "passenger_id": 2,
        "fare": 300,
        "distance": {"km": 28, "mts": 110},
        "time": {"m": 0, "h": 2},
        "from": "C",
        "to": "D",
    },
    4: {
        "driver_id": 2,
        "passenger_id": 2,
        "fare": 300,
        "distance": {"km": 28, "mts": 110},
        "time": {"m": 0, "h": 2},
        "from": "D",
        "to": "C",
    },
}


def get_trip_details(args: dict):

    if args.get("driver_id", None):
        return [
            v for k, v in TRIP_DATA.items() if v["driver_id"] == args.get("driver_id")
        ]
    if args.get("passenger_id", None):
        return [
            v
            for k, v in TRIP_DATA.items()
            if v["passenger_id"] == args.get("passenger_id")
        ]
    return TRIP_DATA.get(args["trip_id"], {})
