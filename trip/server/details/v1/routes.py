from flask import request
from flask_restx import Resource, Namespace, reqparse

from . import controllers as c

api = Namespace("Trip")


@api.route("/details", endpoint="trip_details_v1")
class TripDetails(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("passenger_id", type=int, required=False, nullable=False)
        parser.add_argument("driver_id", type=int, required=False, nullable=False)
        parser.add_argument("trip_id", type=int, required=False, nullable=False)
        args = parser.parse_args()
        return c.get_trip_details(args)
