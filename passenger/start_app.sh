#!/bin/bash
echo "Sleeping 10 seconds to allow for Envoy proxy to start..."
sleep 10

pipenv run gunicorn -c /app/gunicorn.conf.py -b 0.0.0.0:5000 --access-logfile - --error-logfile - 'server:create_app()'