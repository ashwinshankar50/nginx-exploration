import os

FLASK_ENV = os.getenv("FLASK_ENV")
FLASK_APP = os.getenv("FLASK_APP")
PASS_API_HOST = os.getenv("PASS_API_HOST")
DRIV_API_HOST = os.getenv("DRIV_API_HOST")
TRIP_API_HOST = os.getenv("TRIP_API_HOST")
NOTI_API_HOST = os.getenv("NOTI_API_HOST")
