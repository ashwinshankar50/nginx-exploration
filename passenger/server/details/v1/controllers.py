import logging
import requests
from flask import current_app as app

PASSENGER_DATA = {
    1: {
        "name": "X",
        "address": "No1 , 3rd street, chennai-600098",
        "email": "x@gmail.com",
    },
    2: {
        "name": "Y",
        "address": "No1 , 3rd street, chennai-600098",
        "email": "x@gmail.com",
    },
}


def get_passenger_details(passenger_id: int):
    return PASSENGER_DATA.get(passenger_id, {})


def get_trip_details(passenger_id: int):
    result = {}
    trips = []
    pass_details = PASSENGER_DATA.get(passenger_id, {})
    url = f"{app.config.get('TRIP_API_HOST')}/trip/api/v1/details?passenger_id={passenger_id}"
    response = requests.get(url)
    trip_data = response.json()
    result.update(pass_details)
    for trip in trip_data:
        url = f"{app.config.get('DRIV_API_HOST')}/driver/api/v1/details?driver_id={trip['driver_id']}"
        response = requests.get(url)
        trip.update({"driver_details": response.json()})
        print(trip, flush=True)
        trips.append(trip)
    result.update({"trip": trips})
    return result
