from flask import request
from flask_restx import Resource, Namespace, reqparse

from . import controllers as c

api = Namespace("Passenger")


@api.route("/details", endpoint="passenger_details_v1")
class PassengerDetails(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("passenger_id", type=int, required=True, nullable=False)
        args = parser.parse_args()
        return c.get_passenger_details(args["passenger_id"])


@api.route("/trip-info", endpoint="trip_details_v1")
class PassengerDetails(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("passenger_id", type=int, required=True, nullable=False)
        args = parser.parse_args()
        return c.get_trip_details(args["passenger_id"])
