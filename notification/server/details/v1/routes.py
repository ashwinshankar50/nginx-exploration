from flask import request
from flask_restx import Resource, Namespace, reqparse

from . import controllers as c

api = Namespace("Notification")


@api.route("/details", endpoint="notification_details_v1")
class NotificationDetails(Resource):
    def get(self):
        return c.get_notification()
