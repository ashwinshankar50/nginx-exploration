import logging
import requests
from flask import current_app as app

DRIVER_DATA = {
    1: {
        "name": "John A",
        "rating": 4.6,
        "experience": {"y": 1, "m": 2},
        "ride_count": 188,
    },
    2: {
        "name": "Ram Kumar",
        "rating": 3.5,
        "experience": {"y": 5, "m": 2},
        "ride_count": 1006,
    },
}


def get_driver_details(driver_id: int):
    return DRIVER_DATA.get(driver_id, {})
