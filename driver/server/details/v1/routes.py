from flask import request
from flask_restx import Resource, Namespace, reqparse

from . import controllers as c

api = Namespace("Driver")


@api.route("/details", endpoint="driver_details_v1")
class DriverDetails(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("driver_id", type=int, required=True, nullable=False)
        args = parser.parse_args()
        return c.get_driver_details(args["driver_id"])
