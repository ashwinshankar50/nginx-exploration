# Nginx Exploration
#### ``This project is focused, on exploring the concepts of nginx. Please ignore the code structure and other coding related standards.``

#### ``Refrain altering any code or config, inorder to experience the actual setup of the application``
#
## How to start the application
In order to run this application, you need docker in your machine.
```
docker-compose up --build -d
```
Open the browser and go to the URL - http://localhost:80

## Project Overview
#
A (**an incomplete**) cab booking app, with multiple components like, passenger details, user details, notifications, trip details, etc. Each component’s backend are designed (**attempted to design**) in microservice architecture pattern.
Current version of UI has features to get notifications, get user details and trip details (Trip details along with driver details).

Some of the major Nginx concepts focused are:
• Load balancer
• Reverse proxy
• Static content serving

### *Load balancer*

Load balancing configured for notification service. 3 different pods with different service names are created out of which one is a backup server. Which will get requests only if the first 2 are down. Other 2 major servers get requests and its distributed between each other with round robbin mechanism.

### *Reverse proxy*
Location directive configured to direct every request to appropriate services based on the incoming uri.

### *Static content serving*
Few static assets are loaded into the frontend pod and any url ending with the configured extensions tries to serve the requested image. If that image is not available fallback content is configured.

### *Response Caching*

